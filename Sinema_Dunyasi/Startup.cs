﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sinema_Dunyasi.Startup))]
namespace Sinema_Dunyasi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
