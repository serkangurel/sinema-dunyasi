﻿using System.Web;
using System.Web.Mvc;

namespace Sinema_Dunyasi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
