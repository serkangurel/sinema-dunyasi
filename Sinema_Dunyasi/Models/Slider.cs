//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sinema_Dunyasi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Slider
    {
        public int SliderId { get; set; }
        public Nullable<int> FilmId { get; set; }
        public string SliderPath { get; set; }
        public string SliderText { get; set; }
    
        public virtual Film Film { get; set; }
    }
}
