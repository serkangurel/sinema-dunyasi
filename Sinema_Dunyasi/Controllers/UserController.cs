﻿using Sinema_Dunyasi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sinema_Dunyasi.Controllers
{
    public class UserController : Controller
    {
        public static int girisYapildi = 0;
        public static string kullaniciAdi = "";

        private sinema_dunyasiEntities db = new sinema_dunyasiEntities();

        // GET: User
        public ActionResult Kayit()
        {
            return View(new KayitDTO());
        }

        [HttpPost]
        public ActionResult Kayit(KayitDTO kayit)
        {
            Kullanici kullanici = new Kullanici();

            kullanici.KullaniciAdi = kayit.KullaniciAdi;
            kullanici.Email = kayit.Email;
            kullanici.Rol = 2;
            kullanici.Sifre = kayit.Password;
            db.Kullanici.Add(kullanici);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Cikis()
        {
            girisYapildi = 0;
            kullaniciAdi = "";
            return RedirectToAction("Index", "Home");
        }



        public ActionResult Giris()
        {
            return View(new GirisDTO());
        }

        [HttpPost]
        public ActionResult Giris(GirisDTO giris)
        {
            Kullanici kullanici = db.Kullanici.Where(x => x.Email == giris.Email && x.Sifre == giris.Password).FirstOrDefault();

            if (kullanici == null)
            {
                return RedirectToAction("Kayit", "User");
            }

            girisYapildi = (int)kullanici.Rol;
            kullaniciAdi = kullanici.KullaniciAdi;


            return RedirectToAction("Index", "Home");
        }


        public class KayitDTO
        {
            [Required]
            [Display(Name = "KullaniciAdi", ResourceType = typeof(Resources.lang))]
            public string KullaniciAdi { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email", ResourceType = typeof(Resources.lang))]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Sifre", ResourceType = typeof(Resources.lang))]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "SifreTekrar", ResourceType = typeof(Resources.lang))]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public class GirisDTO
        {
            [Required]
            [Display(Name = "Email", ResourceType = typeof(Resources.lang))]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Sifre", ResourceType = typeof(Resources.lang))]
            public string Password { get; set; }


        }
  
        [HttpPost]
        public ActionResult YorumYaz(Yorumlar yorumlar)
        {
            Yorumlar yorum = new Yorumlar();

            yorum.FilmId = yorumlar.FilmId;
            yorum.KullaniciAdi = yorumlar.KullaniciAdi;
            yorum.Yorum = yorumlar.Yorum;
            yorum.Tarih = DateTime.Now;
            
            db.Yorumlar.Add(yorum);
            db.SaveChanges();

            TempData["Mesaj"] = Sinema_Dunyasi.Resources.lang.YorumMesaji;
            return RedirectToAction("Film", "Home", new { FilmId = yorumlar.FilmId });
        }

    }
}