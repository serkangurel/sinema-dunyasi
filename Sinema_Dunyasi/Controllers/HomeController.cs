﻿using Sinema_Dunyasi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sinema_dunyasi.Controllers
{
    public class HomeController : Controller
    {
        private sinema_dunyasiEntities db = new sinema_dunyasiEntities();

        public ActionResult Index()
        {
            ViewBag.Slider = db.Slider.ToList();
            ViewBag.Haber = db.Haber.Take(3).ToList();

            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).Take(3).ToList();
            ViewBag.Yakinda = db.Film.Where(z => z.VizyonTarihi > DateTime.Now).OrderBy(x => x.VizyonTarihi.Value).Take(3).ToList();
            ViewBag.EnIyiFilmler = db.Film.OrderByDescending(x => x.IMDB).Take(3).ToList();

            return View();
        }
        public ActionResult Vizyondakiler()
        {
            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).ToList();
            ViewBag.GelecekFilmler = db.Film.OrderByDescending(z => z.VizyonTarihi > DateTime.Now).Take(4).ToList();
            return View();
        }
        public ActionResult Yakinda()
        {
            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).Take(4).ToList();
            ViewBag.GelecekFilmler = db.Film.OrderByDescending(z => z.VizyonTarihi > DateTime.Now).Take(6).ToList();
            return View();
        }
        public ActionResult EnIyiFilmler()
        {
            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).Take(4).ToList();
            ViewBag.EnIyiFilmler = db.Film.Where(x => x.VizyonTarihi.Value.Year < 2016).OrderByDescending(x => x.IMDB).ToList();
            return View();
        }
        public ActionResult Fragman()
        {
            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).ToList();
            return View();
        }
        public ActionResult Iletisim()
        {
            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Iletisim(Iletisim iletisimform)
        {
            try
            {
                Iletisim _iletisimform = new Iletisim();
                _iletisimform.AdSoyad = iletisimform.AdSoyad;
                _iletisimform.Konu = iletisimform.Konu;
                _iletisimform.Eposta = iletisimform.Eposta;
                _iletisimform.Mesaj = iletisimform.Mesaj;
                _iletisimform.Tarih = DateTime.Now;
                db.Iletisim.Add(_iletisimform);
                db.SaveChanges();
                TempData["Mesaj"] = Sinema_Dunyasi.Resources.lang.FormMesaji;
                ViewBag.Vizyondakiler = db.Film.OrderBy(x => x.VizyonTarihi.Value).ToList();
                return View();

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult Film(int FilmId)
        {

            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).Take(4).ToList();
            ViewBag.Film = db.Film.Where(x => x.FilmId == FilmId).FirstOrDefault(); 
            ViewBag.Yorumlar = db.Yorumlar.Where(x => x.FilmId == FilmId).ToList();

            return View();
        }

        public ActionResult Haber(int HaberId)
        {

            ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).Take(4).ToList();
            ViewBag.Haber = db.Haber.Where(x => x.HaberId == HaberId).FirstOrDefault();

            return View();
        }

        public ActionResult Arama(string searchValue)
        {

            if (!String.IsNullOrEmpty(searchValue))
            {
                ViewBag.Aranan = db.Film.Where(x => x.Ad.ToUpper().Contains(searchValue.ToUpper())
                      || x.Ozet.ToUpper().Contains(searchValue.ToUpper())).ToList();



                ViewBag.Vizyondakiler = db.Film.Where(z => z.VizyonTarihi <= DateTime.Now && z.VizyonTarihi.Value.Year >= 2016).OrderByDescending(x => x.VizyonTarihi.Value).Take(4).ToList();

                return View();
            }

            return RedirectToAction("Index", "Home");

        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

    }

}