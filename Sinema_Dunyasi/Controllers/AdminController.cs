﻿using Sinema_Dunyasi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sinema_Dunyasi.Controllers
{
    public class AdminController : Controller
    {
         
        private sinema_dunyasiEntities db = new sinema_dunyasiEntities();

        // GET: Admin
        public ActionResult Index()
        {
            if(Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }


            ViewBag.Haberler = db.Haber.ToList();
            return View();
        }


        public ActionResult HaberSil(int HaberId)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {

                db.Haber.Remove(db.Haber.First(d => d.HaberId == HaberId));
                db.SaveChanges();
                return RedirectToAction("Index", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }


        [HttpPost]
        public ActionResult HaberEkle(Haber haber, HttpPostedFileBase file)
        {
         
            var fileName = Path.GetFileName(file.FileName);
            var path1 = Path.Combine(Server.MapPath("~/Content/Haber/"), fileName);
            file.SaveAs(path1);

            var path2 = Path.Combine("/Content/Haber/", fileName);

            haber.ResimPath = path2; 

            db.Haber.Add(haber);
            db.SaveChanges();
            return RedirectToAction("Index", "Admin");


        }

        public ActionResult HaberDuzenle(int HaberId)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            var haberDuzenle = db.Haber.Where(x => x.HaberId == HaberId).FirstOrDefault();

            return View(haberDuzenle);
        }

        [HttpPost]
        public ActionResult HaberDuzenle(Haber h, HttpPostedFileBase file)
        {

            var haberDuzenle = db.Haber.Where(x => x.HaberId == h.HaberId).FirstOrDefault();


            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path1 = Path.Combine(Server.MapPath("~/Content/Haber/"), fileName);
                file.SaveAs(path1);

                var path2 = Path.Combine("/Content/Haber/", fileName);
                haberDuzenle.ResimPath = path2;
            }

            haberDuzenle.Tarih = h.Tarih;
            haberDuzenle.Text = h.Text;
            haberDuzenle.Title = h.Title;


            db.SaveChanges();
            return RedirectToAction("Index", "Admin");


        }


        public ActionResult Slider()
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Slider = db.Slider.ToList();
            return View();
        }



        [HttpPost]
        public ActionResult SlideEkle(Slider slide, HttpPostedFileBase file)
        {
            var fileName = Path.GetFileName(file.FileName);
            var path1 = Path.Combine(Server.MapPath("~/Content/Sliders/"), fileName);
            file.SaveAs(path1);

            var path2 = Path.Combine("/Content/Sliders/", fileName);

        
            slide.SliderPath = path2;
            db.Slider.Add(slide);
            db.SaveChanges();
            return RedirectToAction("Slider", "Admin");


        }


        public ActionResult SlideDuzenle(int SlideID)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            var slideDuzenle = db.Slider.Where(x => x.SliderId == SlideID).FirstOrDefault();

            return View(slideDuzenle);
        }



        [HttpPost]
        public ActionResult SlideDuzenle(Slider s, HttpPostedFileBase file)
        {

            var slideDuzenle = db.Slider.Where(x => x.SliderId == s.SliderId).FirstOrDefault();
        

            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path1 = Path.Combine(Server.MapPath("~/Content/Sliders/"), fileName);
                file.SaveAs(path1);

                var path2 = Path.Combine("/Content/Sliders/", fileName);
                slideDuzenle.SliderPath = path2;
            }

            slideDuzenle.SliderText = s.SliderText;
            slideDuzenle.FilmId = s.FilmId;

            db.SaveChanges();
            return RedirectToAction("Slider", "Admin");



        }



        public ActionResult SlideSil(int SlideID)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {

                db.Slider.Remove(db.Slider.First(d => d.SliderId == SlideID));
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }




        public ActionResult Mesajlar()
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Mesajlar = db.Iletisim.ToList();
            return View();
        }

        public ActionResult MesajSil(int MesajId)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                db.Iletisim.Remove(db.Iletisim.First(d => d.MesajId == MesajId));
                db.SaveChanges();
                return RedirectToAction("Mesajlar", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }

        public ActionResult Filmler()
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Filmler = db.Film.ToList();
            return View();
        }

        public ActionResult FilmSil(int FilmId)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                db.Film.Remove(db.Film.First(d => d.FilmId == FilmId));
                db.SaveChanges();
                return RedirectToAction("Filmler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }





        [HttpPost]
        public ActionResult FilmEkle(Film film, HttpPostedFileBase file)
        { 
            var fileName = Path.GetFileName(file.FileName);
            var path1 = Path.Combine(Server.MapPath("~/Content/Afisler/"), fileName);
            file.SaveAs(path1);

            var path2 = Path.Combine("/Content/Afisler/", fileName);

            film.AfisPath = path2;


            db.Film.Add(film);
            db.SaveChanges();
            return RedirectToAction("Filmler", "Admin");


        }


        public ActionResult FilmDuzenle(int FilmId)
        {

            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            var filmDuzenle = db.Film.Where(x => x.FilmId == FilmId).FirstOrDefault();

            return View(filmDuzenle);
        }

        [HttpPost]
        public ActionResult FilmDuzenle(Film f, HttpPostedFileBase file)
        {

            var filmDuzenle = db.Film.Where(x => x.FilmId == f.FilmId).FirstOrDefault();


            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path1 = Path.Combine(Server.MapPath("~/Content/Afisler/"), fileName);
                file.SaveAs(path1);

                var path2 = Path.Combine("/Content/Afisler/", fileName);
                filmDuzenle.AfisPath = path2;
            }

            filmDuzenle.Ad = f.Ad;
            filmDuzenle.VizyonTarihi = f.VizyonTarihi;
            filmDuzenle.Ozet = f.Ozet;
            filmDuzenle.FragmanUrl = f.FragmanUrl;
            filmDuzenle.IMDB = f.IMDB;
            filmDuzenle.Tur = f.Tur;
            filmDuzenle.Oyuncu = f.Oyuncu;
            filmDuzenle.Yonetmen = f.Yonetmen;
            filmDuzenle.Senarist = f.Senarist;
            filmDuzenle.Yapimci = f.Yapimci;

            db.SaveChanges();
            return RedirectToAction("Filmler", "Admin");


        }



        public ActionResult Yorumlar()
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Yorumlar = db.Yorumlar.ToList();
            return View();
        }

        public ActionResult YorumSil(int yorumId)
        {
            if (Sinema_Dunyasi.Controllers.UserController.girisYapildi != 1)
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                db.Yorumlar.Remove(db.Yorumlar.First(d => d.YorumId == yorumId));
                db.SaveChanges();
                return RedirectToAction("Yorumlar", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }




    }
}